Simple PHP logger
=================
Very simple logging created to be like the Android logger class.

Getting Started
---------------
Simply

```php
    require_once "PHPLogger.php";
    $log = new PHPLogger("/path/to/log");
    $tag = "LOG TEST";

    //Print out some debug.
    $log->d($tag, "DEBUGGING FTW");

    //Print some info
    $log->i($tag,"Making cup of tea.");

    //you get the drift...
    $log->w($tag, "OOPS! Spoke too soon, SERVER DOWN!");
```

This will give the following output:

```bash
    [2012-02-15 17:24:40]: [DEBUG][LOG TEST] - DEBUGGING FTW
    [2012-02-15 17:24:40]: [INFO][LOG TEST] - Making cup of tea.
    [2012-02-15 17:24:40]: [WARNING][LOG TEST] - OOPS! Spoke too soon, SERVER DOWN!
```


Take a look at the source for all supported levels, it's not terribly exciting, but there you go..

